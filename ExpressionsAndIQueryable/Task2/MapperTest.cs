﻿//description
/*
 * Используя возможность конструировать Expression Tree и выполнять его код, создайте собственный механизм маппинга (копирующего поля (свойства) одного класса в другой).

Приблизительный интерфейс и пример использования приведен ниже (MapperGenerator – фабрика мапперов, Mapper – класс маппинга). Обратите внимание, что в данном примере создается только новый экземпляр конечного класса, но сами данные не копируются.
 */
using System;
using System.Linq;
using System.Linq.Expressions;
using NUnit.Framework;

namespace Task2
{
    class MapperTest
    {

        public class Mapper<TSource, TDestination>
        {
            Func<TSource, TDestination> mapFunction;

            internal Mapper(Func<TSource, TDestination> func)
            {
                mapFunction = func;
            }

            public TDestination Map(TSource source)
            {
                return mapFunction(source);

            }
        }

        public class MappingGenerator
        {
            public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
            {
                var sourceParam = Expression.Parameter(typeof(TSource), "source");
                var bodyParam = Expression.MemberInit(Expression.New(typeof(TDestination)),
                                                 sourceParam.Type.GetProperties().Select(p => Expression.Bind(typeof(TDestination).GetProperty(p.Name), Expression.Property(sourceParam, p))));
                var mapFunction = Expression.Lambda<Func<TSource, TDestination>>(bodyParam, sourceParam);              
                return new Mapper<TSource, TDestination>(mapFunction.Compile());
            }
        }

        public class Foo
        {
            public string Name { get; set; }
        }


        public class Bar
        {
            public string Name { get; set; }
        }   
     
        [Test]
        public void TestChange()
        {          
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Foo, Bar>();
            var test = new Foo{Name = "WHY SO LONG , Carl" };              
            Assert.AreEqual(test.Name, mapper.Map(test).Name);
        }
    }
}
