﻿// description
/*Задание 1.
Создайте класс-трансформатор на основе ExpressionVisitor, выполняющий следующие 2 вида преобразований дерева выражений:
* Замену выражений вида <переменная> + 1 / <переменная> - 1 на операции инкремента и декремента
* Замену параметров, входящих в lambda-выражение, на константы (в качестве параметров такого преобразования передавать:
o Исходное выражение
o Список пар <имя параметра: значение для замены>
Для контроля полученное дерево выводить в консоль или смотреть результат под отладчиком, использую ExpressionTree Visualizer, а также компилировать его и вызывать полученный метод.
 */

using System;
using System.Linq.Expressions;
using NUnit.Framework;

namespace Task1
{ 

    public class ChangeExpressionTests
    {
        
        [Test]
        public void TransoformToIncrement()
        {
            Expression<Func<int, int>> beforeTransform = a => a - (a + 1) + (a + 1) * (a + 2);
            var afterTransform = new Transformation().VisitAndConvert(beforeTransform, "");

            AfterLogicHelper(beforeTransform, afterTransform, 42);
        }

        [Test]
        public void TransformToDecrement()
        {
            Expression<Func<int, int>> beforeTransform = a => a + (a - 1) + (a + 5) - (a - 1);
            var afterTransform = (new Transformation().VisitAndConvert(beforeTransform, ""));

            AfterLogicHelper(beforeTransform, afterTransform, 42);
        }

        [Test]
        public void DecrementAndIncrement()
        {
            var visitor = new Transformation();

            Expression<Func<int, int>> beforeTransform = a => a + (a + 1) * (a + 5) * (a - 1);
            var afterTransform = (visitor.VisitAndConvert(beforeTransform, ""));
            AfterLogicHelper(beforeTransform, afterTransform, 42);
        }
        
        [Test]
        public void ParametersToConstantsTransformTest()
        {
            Expression<Func<int, int>> beforeTransform = fortyTwo => fortyTwo + (fortyTwo + 1) * (fortyTwo + 5) * (fortyTwo + 1);
            var afterTransform = (new ParamToConstTransofrmation().VisitAndConvert(beforeTransform, ""));
            AfterLogicHelper(beforeTransform,afterTransform,42);
        }

        private void AfterLogicHelper(Expression<Func<int, int>> beforeTransform, Expression<Func<int, int>> afterTransform, int value)
        {
            int beforeTransformResult = beforeTransform.Compile().Invoke(value);
            int afterTransformResult = afterTransform.Compile().Invoke(value);

            Console.WriteLine($"{beforeTransform} = {beforeTransformResult}");
            Console.WriteLine($"{afterTransform} = {afterTransformResult}");

            Assert.AreEqual(beforeTransformResult, afterTransformResult);
        }
    }
}
