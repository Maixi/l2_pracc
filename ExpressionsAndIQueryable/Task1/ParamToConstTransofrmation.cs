﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Task1
{
    public class ParamToConstTransofrmation : ExpressionVisitor
    {
        //https://lostpedia.fandom.com/wiki/The_Numbers
        readonly Dictionary<string, int> _theNumbers = new Dictionary<string, int> {
            { "four", 4 },
            { "eight", 8 },
            { "fifteen", 15 },
            { "sixteen", 16 },
            { "twentyThree", 23 },            
            { "fortyTwo", 42 }
        };

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            return Expression.Lambda<Func<int, int>>(Visit(node.Body), Expression.Parameter(typeof(int)));
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (_theNumbers.Keys.Contains(node.Name) && node.Type == typeof(int))
            {
                return Expression.Constant(_theNumbers.First(x => x.Key == node.Name).Value);
            }

            return base.VisitParameter(node);
        }
    }

}