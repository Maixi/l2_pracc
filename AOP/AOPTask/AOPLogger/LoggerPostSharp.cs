﻿using System;
using PostSharp.Aspects;

namespace AOPLogger
{
    [Serializable]
    public class LoggerPostSharp : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            LoggerNLog.Logger.Trace($"started {args.Method.Name}(Value = {args.Arguments[0]})");
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            LoggerNLog.Logger.Trace($"Result of {args.Method.Name}(Value = {args.Arguments[0]}) execution is {args.ReturnValue}");
        }
    }
}
