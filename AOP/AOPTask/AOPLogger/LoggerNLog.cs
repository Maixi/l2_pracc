﻿using System;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace AOPLogger
{
    public class LoggerNLog
    {
        public static readonly Logger Logger;
        private static readonly string currentDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);

        static LoggerNLog()
        {
            var logConfig = new LoggingConfiguration();
            var target = new FileTarget {
                FileName = Path.Combine(currentDir, @"AOPLog.txt"),
                Layout = "${date} - '${message}' ${onexception:inner=${exception:format=toString}}"
            };

            logConfig.AddTarget("file", target);
            logConfig.AddRuleForAllLevels(target);

            LogManager.Configuration = logConfig;
            Logger = LogManager.GetLogger("LoggerNLog");
        }
    }
}
