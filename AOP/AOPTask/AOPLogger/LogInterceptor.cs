﻿using Castle.DynamicProxy;

namespace AOPLogger
{
    public class LogInterceptor : LoggerNLog, IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            invocation.Proceed();

            Logger.Trace($"{invocation.TargetType}: {invocation.Method.Name}(Value = {invocation.Arguments[0]}) = {invocation.ReturnValue}");
        }
    }
}
