﻿using System.Web.Mvc;
using AOPTaskCodeRewriting.Models;

namespace AOPTaskCodeRewriting.Controllers
{
    public class HomeController : Controller
    {
        private readonly FibonacciEvaluator fibonacciEvaluator = new FibonacciEvaluator();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(uint value)
        {
            var result = fibonacciEvaluator.EvaluateRecursive(value);
            return View(new FibonacciResult { Value = value, Result = result });
        }
    }
}