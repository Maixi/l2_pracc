﻿namespace AOPTaskCodeRewriting.Models
{
    public class FibonacciResult
    {
        public uint Value { get; set; }
        public ulong Result { get; set; }
    }
}