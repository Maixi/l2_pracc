﻿using AOPLogger;

namespace AOPTaskCodeRewriting.Models
{
    public class FibonacciEvaluator
    {
        [LoggerPostSharp]
        public uint Evaluate(uint value)
        {
            if (value == 0 || value == 1) {
                return 1;
            }

            uint fib = 1;
            uint fib2 = 1;

            for (int i = 2; i <= value; i++) {
                var tempFib = fib;
                fib += fib2;
                fib2 = tempFib;
            }

            return fib;
        }

        [LoggerPostSharp]
        public uint EvaluateRecursive(uint value)
        {
            if (value == 0 || value == 1) {
                return 1;
            }

            return EvaluateRecursive(value-1) + EvaluateRecursive(value - 2);
        }
    }
}