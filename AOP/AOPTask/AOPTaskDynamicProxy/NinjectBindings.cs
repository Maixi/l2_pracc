﻿using AOPLogger;
using AOPTaskDynamicProxy.Models;
using Castle.DynamicProxy;
using Ninject.Modules;

namespace AOPTaskDynamicProxy
{
    public class NinjectBindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IFibonacciEvaluator>()
                .ToMethod(ctx =>
                    new ProxyGenerator().CreateInterfaceProxyWithTarget<IFibonacciEvaluator>(new FibonacciEvaluator(), new LogInterceptor()));
        }
    }
}