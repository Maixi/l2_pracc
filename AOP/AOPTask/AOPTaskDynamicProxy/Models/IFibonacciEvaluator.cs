﻿namespace AOPTaskDynamicProxy.Models {
    public interface IFibonacciEvaluator
    {
        uint Evaluate(uint value);
        uint EvaluateRecursive(uint value);
    }
}