﻿using System.Web.Mvc;
using AOPTaskDynamicProxy.Models;

namespace AOPTaskDynamicProxy.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFibonacciEvaluator _fibonacciEvaluator;

        public HomeController(IFibonacciEvaluator fibonacciEvaluator)
        {
            _fibonacciEvaluator = fibonacciEvaluator;
        }

        public ActionResult Index()
        {
            return View(new FibonacciResult());
        }

        [HttpPost]
        public ActionResult Index(uint value)
        {
            var result = _fibonacciEvaluator.EvaluateRecursive(value);
            return View(new FibonacciResult { Value = value, Result = result });
        }
    }
}