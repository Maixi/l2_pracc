﻿using RabbitMQ.Client;

namespace CoreClasses
{
    public static class Channel
    {
        public static IModel CreateChannel(IModel model)
        {
            model.QueueDeclare(queue: "queue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            return model;
        }
    }
}