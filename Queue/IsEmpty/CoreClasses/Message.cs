﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreClasses
{
    public class Message
    {
        public string Text { get; set; }

        public string FileName { get; set; }

        public DateTime TimeCreated { get; set; }

        public string FilePath { get; set; }
    }
}
