﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoreClasses;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Creator
{
    class Program
    {
        static void Main(string[] args)
        {      
            var factory = new ConnectionFactory() {HostName = "localhost"};
            int i = 0;        
            while (true)
            {
                Thread.Sleep(5000);
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        Channel.CreateChannel(channel);
                        var text = "hello";

                        //path to testReulsts directory , where we will store our results 
                        var path = new DirectoryInfo(Environment.CurrentDirectory).Parent.Parent.Parent.FullName +
                                   "\\TestResults\\" + string.Format("{0}.txt",i);
                        File.AppendAllText(path, text);
                        var message = new Message
                        {
                            FileName = i+".txt",
                            Text = text,
                            TimeCreated = DateTime.Now,
                            FilePath = path
                        };
                        var json = JsonConvert.SerializeObject(message);
                        var body = Encoding.UTF8.GetBytes(json);

                        channel.BasicPublish(exchange: "",
                                             routingKey: "queue",
                                             basicProperties: null,
                                             body: body);                                          
                        Console.WriteLine("[x] Sent {0},in {1}", message.FileName,DateTime.Now);
                    }
                }
                i++;
            }
        }
    }
}
