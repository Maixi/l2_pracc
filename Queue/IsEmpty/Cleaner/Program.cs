﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using CoreClasses;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Cleaner
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "queue",
                                         durable: false,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        var data = JsonConvert.DeserializeObject<Message>(message);
                        if (File.Exists(data.FilePath))
                        {
                            File.Delete(data.FilePath);
                        }
                        channel.BasicAck(ea.DeliveryTag, false);
                        Console.WriteLine("[X] Received {0} and deleted in {1}", data.FileName, DateTime.Now);
                    };
                    channel.BasicConsume(queue: "queue",
                                         consumer: consumer);
                    Console.ReadLine();
                }
            }

        }
    }
}