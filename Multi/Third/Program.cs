﻿//description
/*
    * Multithreading V1. Task 3.
    * Write a program , which multiplies two matrices and uses class Parallel
    */
//

using System;
using System.Linq;
using System.Threading.Tasks;

namespace Third
{
    class Program
    {
        static void Main(string[] args)
        {
            const int colCount = 2; 
            const int rowCount = 2; 
            
            // it's elegant !
            // todo: please think about how to make this code more elegant 
            var firstMatrix = InitializeMatrix(colCount, rowCount);
            var secondMatrix = InitializeMatrix(colCount, rowCount);
            var resultMatrix = new double[colCount, rowCount];
            var firstMatrixRows = firstMatrix.GetLength(0); 
            var firstMatrixCols = firstMatrix.GetLength(1);
            // we need it , because we need to multiply two matrices , os we need cols of the second matrix
            // you can found out this algorithm in mentoring program samples in
            // Multi folder / Samples / Topic2.ParallelExtensions / Program.cs line 15;
            var secondMatrixCols = secondMatrix.GetLength(1);
            Parallel.For(0, firstMatrixRows, i =>
                                                      {
                                                          for (var j = 0; j < secondMatrixCols; j++)
                                                          {
                                                              double tmp = 0;
                                                              for (int k = 0; k < firstMatrixCols; k++)
                                                              {
                                                                  tmp += firstMatrix[i, k] * secondMatrix[k, j];
                                                              }

                                                              resultMatrix[i, j] = tmp;
                                                          }
                                                      });
            
            Console.WriteLine(firstMatrix[0,0]);
            Console.WriteLine(secondMatrix[0,0]);
            Console.WriteLine(resultMatrix[0,0]);

            Console.ReadLine();
        }

        private static double[,] InitializeMatrix(int rows, int cols)
        {
            var matrix = new double[rows, cols];              
            var r = new Random();
            // impossible to generate random values from static method Repeat()
            // todo: try to use Enumerable.Repeat to generate the random matrix 
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < cols; j++)
                {
                    //// для понятия того , что это работает можно вместо r.next(100) поставить 2 , тогда у нас будет матрица 2х2 со значением два и тогда это выводимые значения должны будут быть 2,2,8
                    matrix[i, j] = r.Next(100);
                }
            }
            
            return matrix;
        }
    }
}
