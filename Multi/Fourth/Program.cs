﻿//description
/*
    * Multithreading V1. Task 4.
    * Write a program , which recursively creates 10 thread. 
    * Each thread should be with the same body and recieve a state with integer number , decrement in , 
    * print and pass as a state into the newly created thread 
    * Use Thread class for this task and Join for waiting threads
    */
//

using System;
using System.Threading;

namespace Fourth
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadGenerator(10);
            Console.ReadLine();
        }       
        static void ThreadGenerator(int state)
        {
            
            Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} thread started state = {state}");
            state--;
            if (state > 0)
            {
                var thread = new Thread(()=>ThreadGenerator(state));
                thread.Start();
                thread.Join();
                Console.WriteLine($"{Thread.CurrentThread.ManagedThreadId} joined {thread.ManagedThreadId}");
            }

        }
    }
}
