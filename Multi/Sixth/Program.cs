﻿//description
/*
 Write a program which creates two threads and a shared collection: the first one should add 10 elements into the collection and the second should print all elements in the collection after each adding. Use Thread, ThreadPool or Task

classes for thread creation and any kind of synchronization constructions.
 */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Sixth
{
    class Program
    {
        // That's a nice way to organize it. 
        // todo: think about Producer-Consumer pattern here (done)
        // try to implement it with BlockingCollection class and and GetConsumingEnumerable() method
        // and see what happens then.
        public static List<int> sharedCollection = new List<int>();
        static void Main(string[] args)
        {
            Thread p = new Thread(new ThreadStart(Program.Produce));
            Thread c = new Thread(new ThreadStart(Program.Consume));
            p.Start();
            c.Start();           
            Console.ReadLine();
        }

        private static Semaphore isFull = new Semaphore(0, 1);
        private static Semaphore isEmpty = new Semaphore(1, 1);

        static void Produce()
        {
            for (int i = 0; i < 10; i++)
            {
                isFull.WaitOne();
                sharedCollection.Add(i);
                isEmpty.Release();
            }
        }

        static void Consume()
        {
            for (int i = 0; i < 10; i++)
            {
                isEmpty.WaitOne();
                Console.WriteLine(String.Join(", ", sharedCollection));
                isFull.Release();
            }
        }
    }
}
