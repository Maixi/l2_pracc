﻿//description
/*
    * Multithreading V1. Task 2.
    * Write a program, which creates a chain of four Tasks.First Task - creates an array of 10 random integer
    * Second Task - multiplies this array with another random integer
    * Third Task - sorts this array by ascending.
    * Fourth Task - calculates the average value.
    * All this tasks should print the values to console
    */
//
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Multi
{
    class Program
    {      
        static void Main(string[] args)
        {
            Task.Factory.StartNew(() => CreateArray(10))
                .ContinueWith(task => Multiply(task.Result))
                .ContinueWith(task => Sort(task.Result))
                .ContinueWith(task => Console.WriteLine($"AVG: {task.Result.Average()}"))
                .Wait();

            Console.ReadLine();
        }

        private static int[] CreateArray(int arrayLength)
        {
            var blessRNG = new Random();            
            var array = Enumerable.Range(0, arrayLength).Select(x => blessRNG.Next(0, 20)).ToArray();
            Console.WriteLine($"Your Values: {string.Join(",", array)}");
            return array;
        }

        private static int[] Multiply(int[] array)
        {
            var blessRNG = new Random();
            var param = blessRNG.Next(0, 20);
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = array[i] * param;
            }
            Console.WriteLine($"multiplied by {param}: {string.Join(",", array)}");
            return array;
        }

        private static int[] Sort(int[] array)
        {
            Console.WriteLine($"Sorted array: {string.Join(",", array.OrderBy(a => a))}");
            return array;
        }
    }
}
