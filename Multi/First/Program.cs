﻿//description
/*
    * Multithreading V1. Task 1.
    * Write a program, which creates an array of 100 Tasks, runs them and wait all of them are not finished.
    * Each Task should iterate from 1 to 1000 and print into the console the following string:
    *      “Task #0 – {iteration number}”
    */
//
using System;
using System.Threading.Tasks;

namespace First
{     
    class Program
    {
        static void Main(string[] args)
        {
            var taskArray = new Task[100];

            for (var i = 0; i < taskArray.Length; i++)
            {
                taskArray[i] = Task.Factory.StartNew((numberOfTheTask) =>
                                                     {
                                                         for (int j = 0; j < 1000; j++)
                                                         {
                                                             Console.WriteLine($"Task #{numberOfTheTask} - {j}");
                                                         }
                                                     }, i);
            }
            Task.WaitAll(taskArray);
            Console.ReadLine();
        }
    }
}
