﻿// description
/*
 Create a Task and attach continuations to it according to the following criteria:

a. Continuation task should be executed regardless of the result of the parent task.

b. Continuation task should be executed when the parent task finished without success.

c. Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation

d. Continuation task should be executed outside of the thread pool when the parent task would be cancelled

Demonstrate the work of the each case with console utility.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Seventh
{
    class Program
    {
        const string AVariant = "Continuation task should be executed regardless of the result of the parent task.";
        const string BVariant = "Continuation task should be executed when the parent task finished without success.";
        const string CVariant = "Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation";
        const string DVariant = "Continuation task should be executed outside of the thread pool when the parent task would be cancelled";

        static void Main(string[] args)
        {
            SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            tokenSource.Cancel();          
         
            Task.Factory.StartNew(() => { }).
                ContinueWith(task => 
                             {
                    Console.WriteLine(AVariant);
                    throw new Exception();
                             }, 
                             TaskContinuationOptions.None)

                .ContinueWith(x => 
                              {
                    Console.WriteLine(BVariant);
                    throw new Exception();
                              }, 
                              TaskContinuationOptions.OnlyOnFaulted)

                .ContinueWith(x => 
                    {
                        tokenSource.Cancel();
                        Console.WriteLine(CVariant);
                    }, 
                        token, TaskContinuationOptions.OnlyOnFaulted 
                             | TaskContinuationOptions.ExecuteSynchronously,
                        TaskScheduler.Default)

                .ContinueWith(x =>
                    {
                        Console.WriteLine(DVariant);
                    }, 
                        CancellationToken.None,
                        TaskContinuationOptions.OnlyOnCanceled,
                        TaskScheduler.FromCurrentSynchronizationContext());

            Thread.Sleep(10000);

            Console.ReadLine();
        }
    }
}
