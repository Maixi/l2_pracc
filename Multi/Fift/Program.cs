﻿//description
/*
    * Multithreading V1. Task 5.
    * Write a program , which recursively creates 10 thread. 
    * Each thread should be with the same body and recieve a state with integer number , decrement in , 
    * print and pass as a state into the newly created thread 
    * Use Thread 
    */
//

using System;
using System.Threading;

namespace Fift
{
    class Program
    {
        private static int state = 10;
        // todo: please add a modificator implicitly
        static readonly Semaphore Semaphore = new Semaphore(2, 2);
        private static readonly ManualResetEvent resetEvent = new ManualResetEvent(false);
       
        static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem(Countdown);
            resetEvent.WaitOne();
            
            Console.ReadLine();
        }

        private static void Countdown(Object stateInfo)
        {
            Semaphore.WaitOne();
            state--;
            Console.WriteLine($"Current state is: {state}");
            if (state > 0)
                ThreadPool.QueueUserWorkItem(Countdown);
            else
                resetEvent.Set();

            Thread.Sleep(1000);
            Semaphore.Release();
        }

    }
}
