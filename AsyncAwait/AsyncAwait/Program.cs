﻿//description
/*
 Напишите консольное приложение для асинхронного расчета суммы целых чисел от 0 до N. N задается пользователем из консоли.
 
 Пользователь вправе внести новую границу в процессе вычислений, что должно привести к перезапуску расчета.
 // todo: please, add a proper delay to imitate the load and check how it all works.
 
 Это не должно привести к «падению» приложения.
 */

using System;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait
{
    class Program
    {
        // todo: please, add the task text and the variant (for each task)
        static void Main(string[] args)
        {
            Action<string, CancellationToken> getCount = async (input, token) =>
            {
                var result = await StartCountAsync(input, token);
                Console.WriteLine(result);
            };

            Console.WriteLine("Введите число , чтобы выйти нажмите Q");
            var userInput = Console.ReadLine();

            var cancellationToken = new CancellationTokenSource();
            while (userInput != null && !userInput.ToUpper().StartsWith("Q"))
            {
                getCount(userInput, cancellationToken.Token);
                userInput = Console.ReadLine();
                cancellationToken.Cancel();
                cancellationToken.Dispose();
                cancellationToken = new CancellationTokenSource();
            }

            // todo: the cancellation token should be disposed in the end
            // 'using' construction will help here
        }

        private static async Task<string> StartCountAsync(string userNumberText, CancellationToken token)
        {
            try
            {
                return await Task<string>.Factory.StartNew(() => GetCounter(userNumberText, token), token);
            }
            catch // todo: please, catch only a certain type of exception
            {
                return "Вы отменили , а я так старался считать :( ";
            }
        }

        private static string GetCounter(string userNumberText, CancellationToken token)
        {                     
            long.TryParse(userNumberText, out long userNumberlong);

            return string.Concat("Ваше число : ", userNumberlong, " Сумма : ", Count(userNumberlong, token));
        }

        private static long Count(long maxNumber, CancellationToken token)
        {
            long result = 0;

            for (int i = 0; i <= maxNumber; i++)
            {
                token.ThrowIfCancellationRequested();             
                result += i;
            }            
            return result;
        }
    }
}
