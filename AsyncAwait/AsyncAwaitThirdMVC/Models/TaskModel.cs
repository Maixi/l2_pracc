﻿using System;
using System.Net;

namespace AsyncAwaitThirdMVC.Models
{
    public class TaskModel
    {
        public string Site { get; set; }
        public Guid Id { get; set; }
        public WebClient Client { get; set; }
        public TaskModel(UrlModel model)
        {
            Site = model.Site;
            Id = model.Id;
        }
    }
}