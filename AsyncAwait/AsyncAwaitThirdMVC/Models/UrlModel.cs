﻿using System;

namespace AsyncAwaitThirdMVC.Models
{
    public class UrlModel
    {
        public string Site { get; set; }
        public Guid Id { get; set; }
        public string Content { get; set; }
    }
}