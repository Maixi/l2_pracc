﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading.Tasks;

namespace AsyncAwaitThirdMVC.Models
{
    public class Downloader
    {
        private static readonly ConcurrentDictionary<Guid, TaskModel> DownloaderManager
            = new ConcurrentDictionary<Guid, TaskModel>();

        public static void CreateNew(UrlModel model)
        {
            model.Id = Guid.NewGuid();
            var taskmodel = new TaskModel(model);
            taskmodel.Client = new WebClient();
            
            DownloaderManager.TryAdd(model.Id, taskmodel);
        }

        public static async Task<string> StartNew(Guid id)
        {
            TaskModel taskmodel;
            DownloaderManager.TryRemove(id, out taskmodel);
            var client = taskmodel.Client;
            var uri = new Uri(taskmodel.Site);
            taskmodel = null;
            return await client.DownloadStringTaskAsync(uri);
        }

        public static UrlModel Cancel(UrlModel model)
        {
            TaskModel taskmodel;
            DownloaderManager.TryGetValue(model.Id, out taskmodel);
            if (taskmodel?.Client != null)
                taskmodel.Client.CancelAsync();
            model.Content = "Canceled";

            return model;
        }
    }
}