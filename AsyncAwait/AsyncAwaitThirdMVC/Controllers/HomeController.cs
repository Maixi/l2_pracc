﻿using System.Web.Mvc;

namespace AsyncAwaitThirdMVC.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }     
    }
}