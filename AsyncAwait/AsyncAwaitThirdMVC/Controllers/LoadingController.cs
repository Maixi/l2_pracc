﻿//description
/*
 *Напишите простейший менеджер закачек. Пользователь задает адрес страницы, которую необходимо загрузить.
 * В процессе загрузки пользователь может ее отменить. 
 * Пользователь может задавать несколько источников для закачки. todo: this is not implemented
 * Скачивание страниц не должно блокировать интерфейс приложения.
 */

using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using AsyncAwaitThirdMVC.Models;

namespace AsyncAwaitThirdMVC.Controllers
{
    public class LoadingController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        } // todo: please, use some common code style here
        [HttpPost]
        public ActionResult DownloadSite(UrlModel model)
        {
            Downloader.CreateNew(model);

            return PartialView(model);
        }
        public ActionResult Cancel(UrlModel model)
        {
            return PartialView(Downloader.Cancel(model));
        }

        public async Task<string> StartDownload(Guid id)
        {
            return await Downloader.StartNew(id);
        }
    }
}