﻿//description
/*
Напишите простейший магазин по заказу еды.
Пользователь может выбрать товар, и он добавляется в корзину.
При изменении товаров происходит автоматический пересчет стоимости. 
Любые действия пользователя с меню или корзиной не должны влиять на производительность UI (замораживать).
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitSecond
{
    class Program
    {
        private static readonly Dictionary<string, decimal> Bin = new Dictionary<string, decimal>();
        private static decimal _totalprice = 0;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Write product name and price splited by space");
                var input = Console.ReadLine()?.Trim().Split(' ');
                if (string.IsNullOrEmpty(input?[0]) || !decimal.TryParse(input?[1], out decimal price))
                {
                    Console.WriteLine("incorrect input. try again");
                    continue;
                }

                AddProductToBinAsync(input[0], price);
            }
        }

        static async Task AddProductToBinAsync(string product, decimal price)
        {
            try
            {
                await Task.Run(() => AddProductToBin(product, price)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Can't add {product} with price {price}. " + e.Message);
            }
        }

        private static void AddProductToBin(string product, decimal price)
        {
            if (price < 0)
            {
                throw new Exception("Price cant be less than 0");
            }
            Console.WriteLine($"Adding {product} to bin");
            Thread.Sleep(10000);

            if (Bin.ContainsKey(product))
            {
                Bin[product] += price;
            }
            else
            {
                Bin.Add(product, price);
            }

            _totalprice = Bin.Values.Sum();
            Console.WriteLine($"{product} was added to bin. Total price is {_totalprice}");
        }
    }
}
