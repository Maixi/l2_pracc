﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;

namespace AOPTask
{
    public static class PdfService
    {
        private static string pdfFileName = @"NewPdfDocument.pdf";

        public static void ConvertToPdf(string dir, ImageFiles filesToConvert, out bool converted)
        {
            if (!IsProperFiles(dir, filesToConvert)) {
                converted = false;
                return;
            }

            var document = new Document();
            var documentName = FileNameService.NextAvailableFilename(Path.Combine(dir, pdfFileName));

            CombineDocument(document, filesToConvert.Files.Select(f => f.FileName).ToList());

            var render = new PdfDocumentRenderer { Document = document };

            render.RenderDocument();
            render.Save(documentName);

            foreach (var file in Directory.EnumerateFiles(dir)) {
                if (Path.GetExtension(file) != ".pdf" && FileService.TryOpen(file, 3))
                    File.Delete(file);
            }

            converted = true;
        }

        private static bool IsProperFiles(string dir, ImageFiles filesToCheck)
        {
            return filesToCheck != null 
                && filesToCheck.Files.Any()
                && IsAllFilesHere(dir, filesToCheck)
                && IsExtensionCorrect(filesToCheck);
        }

        private static bool IsExtensionCorrect(ImageFiles filesToCheck)
        {
            return filesToCheck.Files
                .All(file => !String.IsNullOrWhiteSpace(file?.FileName)
                             && (Path.GetExtension(file.FileName).ToLower() == ".jpg" ||
                                 Path.GetExtension(file.FileName).ToLower() == ".jpeg"));
        }

        private static bool IsAllFilesHere(string directory, ImageFiles filesToCheck)
        {
            //var allOfList1IsInList2 = list1.Intersect(list2).Count() == list1.Count();
            var result = filesToCheck.Files.Select(x => x.FileName)
                             .Intersect(Directory.EnumerateFiles(directory))
                             .Count() == filesToCheck.Files.Count();

            if (!result) {
                Thread.Sleep(2000);

                result = filesToCheck.Files.Select(x => x.FileName)
                             .Intersect(Directory.EnumerateFiles(directory))
                             .Count() == filesToCheck.Files.Count();
            }

            return result;
        }

        private static void CombineDocument(Document document, IEnumerable<string> files)
        {
            var section = document.AddSection();
            foreach (var file in files) {
                var img = section.AddImage(file);
                img.Height = document.DefaultPageSetup.PageHeight;
                img.Width = document.DefaultPageSetup.PageWidth;

                section.AddPageBreak();
            }
        }
    }
}
