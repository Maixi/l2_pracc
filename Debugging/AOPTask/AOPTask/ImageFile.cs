﻿using System.Collections.Generic;

namespace AOPTask
{
    public class ImageFile
    {
        public string FileName { get; set; }
    }

    public class ImageFiles
    {
        public List<ImageFile> Files { get; set; }
    }
}
