﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Threading;
using Topshelf;

namespace AOPTask
{
    internal class FileService //: ServiceControl
    {
        private const string MessageQueueName = @".\private$\MyQueue";
        private const string InputMessageLabel = @"Files in input folder";
        private const string OutputMessageLabel = @"Files in output folder";
        //static string currentDir = Environment.CurrentDirectory;
        string inDir;// = Path.Combine(currentDir, "Input");
        string outDir;// = Path.Combine(currentDir, "Output");
        public static MessageQueue MyMessageQueue { get; set; }

        FileSystemWatcher watcher;

        Thread queueMonitorThread;
        Thread inputWatcherThread;

        readonly ManualResetEvent stopWorkEvent;
        readonly AutoResetEvent newFileEvent;

        readonly ManualResetEvent filesMovedEvent;
        readonly ManualResetEvent filesConvertedEvent;

        public FileService(string inDir, string outDir)
        {
            if (!Directory.Exists(inDir)) {
                Directory.CreateDirectory(inDir);
                this.inDir = inDir;
            }

            if (!Directory.Exists(outDir)) {
                Directory.CreateDirectory(outDir);
                this.outDir = outDir;
            }

            MyMessageQueue = MessageQueue.Exists(MessageQueueName)
                ? new MessageQueue(MessageQueueName)
                : MessageQueue.Create(MessageQueueName);

            watcher = new FileSystemWatcher(inDir);
            watcher.Created += Watcher_Created;

            queueMonitorThread = new Thread(ReceiveMessage);
            inputWatcherThread = new Thread(WatchInput);

            stopWorkEvent = new ManualResetEvent(false);
            newFileEvent = new AutoResetEvent(false);

            filesMovedEvent = new ManualResetEvent(true);
            filesConvertedEvent = new ManualResetEvent(true);
        }

        private void ReceiveMessage()
        {
            MyMessageQueue.Formatter = new XmlMessageFormatter(new[] { typeof(ImageFiles) });

            do {
                using (MyMessageQueue) {
                    var message = MyMessageQueue.Receive();
                    if (message == null) return;

                    if (message.Label == InputMessageLabel) {
                        Console.WriteLine("Found files in input folder");

                        ThreadPool.QueueUserWorkItem(MoveFiles, message.Body as ImageFiles);
                        Console.WriteLine("Files have been moved");
                    } else if (message.Label == OutputMessageLabel) {
                        Console.WriteLine("Found files in output folder");

                        ThreadPool.QueueUserWorkItem(ConvertToPdf, message.Body as ImageFiles);
                        Console.WriteLine("Files have been converted");
                    }
                }
            } while (!stopWorkEvent.WaitOne(TimeSpan.Zero));
        }

        private void WatchInput(object obj)
        {
            do {
                var files = Directory.GetFiles(inDir);
                int currentFileNumber, previousFileNumber;

                if (files.Length > 0) {
                    files = files.OrderBy(FileNameService.GetFileNumber).ToArray();
                    var filesToSend = new List<ImageFile>();
                    currentFileNumber = previousFileNumber = FileNameService.GetFileNumber(files[0]);

                    for (int i = 0; i < files.Length; i++) {
                        if (stopWorkEvent.WaitOne(TimeSpan.Zero)) {
                            return;
                        }

                        currentFileNumber = FileNameService.GetFileNumber(files[i]);

                        if (currentFileNumber - previousFileNumber > 1) {
                            break;
                        }

                        filesToSend.Add(new ImageFile { FileName = files[i] });
                        previousFileNumber = currentFileNumber;
                    }

                    using (MyMessageQueue) {
                        MyMessageQueue.Send(new ImageFiles { Files = filesToSend }, InputMessageLabel);
                    }
                }
            } while (WaitHandle.WaitAny(new WaitHandle[] { stopWorkEvent, newFileEvent }, 1000) != 0);
        }

        private void MoveFiles(ImageFiles filesToMove)
        {
            try {
                if (filesToMove == null || !filesToMove.Files.Any()) {
                    return;
                }

                var movedFiles = new ImageFiles { Files = new List<ImageFile>() };

                foreach (var file in filesToMove.Files) {
                    if (stopWorkEvent.WaitOne(TimeSpan.Zero)) {
                        return;
                    }

                    var inFile = file;
                    var outFile = Path.Combine(outDir, Path.GetFileName(inFile.FileName));

                    if (TryOpen(inFile.FileName, 3)) {
                        File.Move(inFile.FileName, outFile);
                        movedFiles.Files.Add(new ImageFile { FileName = outFile });
                    }
                }

                if (movedFiles.Files.Any()) {
                    using (MyMessageQueue) {
                        MyMessageQueue.Send(movedFiles, OutputMessageLabel);
                    }
                }
            } catch (Exception e) {
                // ignored
            }
        }

        private void MoveFiles(object obj)
        {
            try {
                filesMovedEvent.Reset();
                if (stopWorkEvent.WaitOne(TimeSpan.Zero)) {
                    filesMovedEvent.Set();
                    return;
                }

                var filesToMove = obj as ImageFiles;
                if (filesToMove == null || !filesToMove.Files.Any()) return;

                bool moved = false;
                var movedFiles = new ImageFiles { Files = new List<ImageFile>() };

                foreach (var file in filesToMove.Files) {
                    moved = false;
                    if (stopWorkEvent.WaitOne(TimeSpan.Zero))
                        return;

                    var inFile = file;
                    var outFile = Path.Combine(outDir, Path.GetFileName(inFile.FileName));

                    if (TryOpen(inFile.FileName, 3)) {
                        File.Move(inFile.FileName, outFile);
                        moved = true;
                        movedFiles.Files.Add(new ImageFile { FileName = outFile });
                    }
                }

                if (moved && movedFiles.Files.Any()) {
                    using (MyMessageQueue) {
                        MyMessageQueue.Send(movedFiles, OutputMessageLabel);
                    }
                    filesMovedEvent.Set();
                }
            } finally {
                filesMovedEvent.Set();
            }
        }

        private void ConvertToPdf(object obj)
        {
            try {
                filesConvertedEvent.Reset();

                if (stopWorkEvent.WaitOne(TimeSpan.Zero)) {
                    filesConvertedEvent.Set();
                    return;
                }

                var filesToConvert = obj as ImageFiles;
                if (filesToConvert == null || !filesToConvert.Files.Any()) return;

                bool converted;
                PdfService.ConvertToPdf(outDir, filesToConvert, out converted);

                if (converted) filesConvertedEvent.Set();
            } finally {
                filesConvertedEvent.Set();
            }
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            newFileEvent.Set();
        }

        internal bool Start()
        {
            inputWatcherThread.Start();
            watcher.EnableRaisingEvents = true;
            queueMonitorThread.Start();
            return true;
        }

        internal bool Stop()
        {
            watcher.EnableRaisingEvents = false;
            stopWorkEvent.Set();
            inputWatcherThread.Join();
            queueMonitorThread.Join();

            filesMovedEvent.WaitOne();
            filesConvertedEvent.WaitOne();
            return true;
        }

        public static bool TryOpen(string fileName, int tryCount)
        {
            if (!File.Exists(fileName)) return false;

            for (int i = 0; i < tryCount; i++) {
                try {
                    var file = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                    file.Close();

                    return true;
                } catch (IOException) {
                    Thread.Sleep(5000);
                }
            }

            return false;
        }
    }
}
