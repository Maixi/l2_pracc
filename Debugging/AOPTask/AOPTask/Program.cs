﻿using System;
using System.Diagnostics;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;
using Topshelf;

namespace AOPTask
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
                var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
                var inputDirectory = Path.Combine(currentDir, "Input");
                var outputDirectory = Path.Combine(currentDir, "Output");

                if (!Directory.Exists(inputDirectory))
                    Directory.CreateDirectory(inputDirectory);

                if (!Directory.Exists(outputDirectory))
                    Directory.CreateDirectory(outputDirectory);

                var conf = new LoggingConfiguration();
                var fileTarget = new FileTarget
                {
                    Name = "Default",
                    FileName = Path.Combine(currentDir, "log.txt"),
                    Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
                };

                conf.AddTarget(fileTarget);
                conf.AddRuleForAllLevels(fileTarget);

                var logFactory = new LogFactory(conf);

                HostFactory.Run(hostConf =>
                {
                    hostConf.Service<FileService>(service =>
                    {
                        service.ConstructUsing(s => new FileService(inputDirectory, outputDirectory));
                        service.WhenStarted(s => s.Start());
                        service.WhenStopped(s => s.Stop());
                    }).UseNLog(logFactory);

                    //Setup Account that window service use to run.
                    hostConf.SetServiceName("MyWindowService");
                    hostConf.SetDisplayName("MyWindowService");
                    hostConf.SetDescription("My .Net windows service");
                    hostConf.StartAutomaticallyDelayed();
                    hostConf.RunAsLocalService();
                    //hostConf.StartManually();

                });

                //HostFactory.Run(hostConf => {
                //    hostConf.Service<FileService>();

                //    //Setup Account that window service use to run.
                //    hostConf.SetServiceName("MyWindowService");
                //    hostConf.SetDisplayName("MyWindowService");
                //    hostConf.SetDescription("My .Net windows service");
                //    hostConf.StartAutomaticallyDelayed();
                //    hostConf.RunAsLocalService();
                //    //hostConf.StartManually();

                //});
            } catch (Exception ex) {
                EventLog.WriteEntry("Application", ex.ToString(), EventLogEntryType.Error);
            }
        }
    }
}

