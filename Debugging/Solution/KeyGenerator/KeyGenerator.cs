﻿using System;
using System.Linq;
using System.Net.NetworkInformation;

namespace KeyGenerator
{
    public static class KeyGenerator
    {
        private static byte[] dateBytes { get; set; }

        public static string GetKey(DateTime date)
        {
            SetDateBytes(date);

            var networkInterface = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault();
            if (networkInterface == null) {
                return "Something wrong with the NetworkInterface";
            }

            var generatedKey = networkInterface.GetPhysicalAddress()
                .GetAddressBytes()
                .Select(PerformXOR)
                .Select(Multiply)
                .ToArray();

            return string.Join("-", generatedKey);
        }

        private static void SetDateBytes(DateTime date)
        {
            dateBytes = BitConverter.GetBytes(date.Date.ToBinary());
        }

        private static int PerformXOR(byte b, int i)
        {
            var dateByte = dateBytes[i];
            var res = (b ^ dateByte);
            return res;
        }

        private static int Multiply(int i)
        {
            var res = i;
            if (i <= 0x3e7) {
                res = (i * 10);
            }
            return res;
        }
    }
}
