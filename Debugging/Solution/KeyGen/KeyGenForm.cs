﻿using System;
using System.Windows.Forms;

namespace KeyGen
{
    public partial class KeyGenForm : Form
    {
        public KeyGenForm()
        {
            InitializeComponent();
            KeyTextBox.Text = KeyGenerator.KeyGenerator.GetKey(DateTime.Now);
        }

        private void label1_Click(object sender, EventArgs e) { }

        private void GenerateButton_Click(object sender, EventArgs e)
        {
            var date = DatePicker.Value;
            KeyTextBox.Text = KeyGenerator.KeyGenerator.GetKey(date);
        }
    }
}
