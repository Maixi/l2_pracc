﻿using System;
using System.Runtime.InteropServices;

namespace PowerManagmentCOM
{
    [ComVisible(true)]
    [Guid("6307091E-206E-4335-B64B-D1267B45343C")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IPowerManagerCom
    {
        DateTime GetLastSleepTime();

        DateTime GetLastWakeTime();

        bool GetSystemBatteryState();

        int GetSystemCoolingMode();
    }
}