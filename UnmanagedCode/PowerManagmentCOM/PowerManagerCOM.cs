﻿using System;
using System.Runtime.InteropServices;
using PowerManagment;

namespace PowerManagmentCOM
{
    [ComVisible(true)]
    [Guid("EDFCCFCD-7578-4E0F-98D0-145A605B3B15")]
    [ClassInterface(ClassInterfaceType.None)]
    public sealed class PowerManagerCom : IPowerManagerCom
    {
        private readonly PowerManager _powerManager;

        public PowerManagerCom()
        {
            _powerManager = new PowerManager();
        }

        public DateTime GetLastSleepTime()
        {
            return _powerManager.GetLastSleepTime();
        }

        public DateTime GetLastWakeTime()
        {
            return _powerManager.GetLastWakeTime();
        }

        public bool GetSystemBatteryState()
        {
            return _powerManager.GetSystemBatteryState().Present;
        }

        public int GetSystemCoolingMode()
        {
            return _powerManager.GetSystemPowerInformation().CoolingMode;
        }
    }
}
