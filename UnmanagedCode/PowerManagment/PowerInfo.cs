﻿using System.Runtime.InteropServices;

namespace PowerManagment
{
    [StructLayout(LayoutKind.Sequential)]
    [ComVisible(true)]
    public struct PowerInfo
    {
        public uint MaxIdlenessAllowed;
        public uint Idleness;
        public uint TimeRemaining;
        public sbyte CoolingMode;
    }
}
