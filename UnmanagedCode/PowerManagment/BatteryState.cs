﻿using System.Runtime.InteropServices;

namespace PowerManagment
{
    [StructLayout(LayoutKind.Sequential)]
    [ComVisible(true)]
    public struct BatteryState
    {
        public bool AcOnLine;
        public bool BatteryPresent;
        public bool Present;
        public bool Discharging;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public bool[] Spare1;
        public int MaxCapacity;
        public int RemainingCapacity;
        public int Rate;
        public int EstimatedTime;
        public int DefaultAlert1;
        public int DefaultAlert2;
    }
}
